/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution")

module.exports = {
   root: true,
   extends: [
      "plugin:vue/vue3-recommended",
      "eslint:recommended",
      "@vue/eslint-config-typescript/recommended",
      "@vue/eslint-config-prettier"
   ],
   parserOptions: {
      ecmaVersion: "latest"
   },
   env: {
      node: true,
      "vue/setup-compiler-macros": true
   },
   rules: {
      "comma-dangle": ["error", "never"],
      semi: ["error", "never"],
      "vue/no-deprecated-slot-attribute": "off"
   }
}
