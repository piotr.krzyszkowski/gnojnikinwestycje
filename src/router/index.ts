import { createRouter, createWebHistory } from "vue-router"
import HomeView from "../views/Investments/InvestmentsView.vue"
import SportObjectReservationView from "../views/reservations/SportObjectReservationView.vue"
import SidewalksView from "../views/Sidewalks/SidewalksView.vue"

const router = createRouter({
   history: createWebHistory(),
   routes: [
      {
         path: "/",
         name: "home",
         component: HomeView
      },
      // {
      //   path: '/about',
      //   name: 'about',
      //   // route level code-splitting
      //   // this generates a separate chunk (About.[hash].js) for this route
      //   // which is lazy-loaded when the route is visited.
      //   component: () => import('../views/AboutView.vue')
      // }
      {
         path: "/sport-object-reservation",
         name: "sport-object-reservation",
         component: SportObjectReservationView
      },
      {
         path: "/sidewalks",
         name: "sidewalks",
         component: SidewalksView
      }
   ]
})

export default router
