import { defineStore } from "pinia"
import { readItems } from "@directus/sdk"
import { client } from "@/utils/directusClient"
import { ref } from "vue"
export interface IInvestmentCondition {}
export const useInvestmentCondition = defineStore("investmentCondition", () => {
   const investmentCondition = ref<unknown[]>([])
   async function fetch() {
      const response = await client.request(
         readItems("investment_condition" as unknown as string, {
            fields: ["*.*.*"]
         })
      )
      investmentCondition.value = response
      console.log(investmentCondition.value)
   }

   return { fetch, investmentCondition }
})
