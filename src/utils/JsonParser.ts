import { useReservation, type IGroup, type IReservation, type ISportObject } from "@/views/reservations/store.ts"
export function csvJSON(csv: string): IReservation[] {
   const store = useReservation()
   const groups: IGroup[] = store.groups
   const sport_objects: ISportObject[] = store.sportObject
   const lines = csv.split("\n").map((line) => line.trim())
   const result: any[] = []
   const headers = lines[0].split(",").map((header) => header.trim())
   for (let i = 1; i < lines.length; i++) {
      if (!lines[i]) continue
      const obj: any = {}
      const currentline = lines[i].match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g)

      if (currentline) {
         for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j] ? currentline[j].replace(/^"|"$/g, "").trim() : ""
         }
         result.push(obj)
      }
   }

   let formaterResult = result.map((res) => {
      const group: IGroup = groups.find((group) => group.name === res['"Drużyna"']) as unknown as IGroup
      console.log(res['"Miejsce"'])
      const sport_object = sport_objects.find((object) =>
         res['"Miejsce"'].includes(object.name)
      ) as unknown as ISportObject
      return {
         date: res['"Data rozpoczęcia"'].substring(0, 10),
         hour_end: res['"Data zakończenia"'].substring(11),
         hour_start: res['"Data rozpoczęcia"'].substring(11),
         name: res['"Tytuł"'],
         object_id: sport_object
            ? {
                 id: sport_object.id,
                 name: sport_object.name,
                 place: sport_object.place,
                 color: sport_object.color
              }
            : undefined,
         group_id: group
            ? {
                 id: group.id,
                 name: group.name,
                 color: group.color,
                 club: group.club
              }
            : undefined
      }
   })

   formaterResult = formaterResult.filter((res) => {
      return res.hour_start !== "" && res.hour_end !== ""
   })

   return formaterResult
}
