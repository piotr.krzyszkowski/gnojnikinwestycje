import { createDirectus, rest } from "@directus/sdk"

export const client = createDirectus("https://orca-app-8y4zd.ondigitalocean.app/").with(rest())
