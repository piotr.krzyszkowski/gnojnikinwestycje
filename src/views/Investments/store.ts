import { ref, computed } from "vue"
import { defineStore } from "pinia"
import { readItems } from "@directus/sdk"
import { client } from "@/utils/directusClient"

export interface IInvestmens {
   active: boolean
   cost: number
   date_created: string
   date_updated: string
   external_financing_percent: number
   geo_place: {
      coordinates: number[]
      type: string
   }
   name: string
   place: {
      id: number
      name: string
      sort: null
   }
   type: {
      id: number
      name: string
      sort: null
   }
   year: string
}
export interface IVilages {
   id: number
   name: string
}
export interface Itypes {
   id: number
   name: string
}

export const useInvestments = defineStore("invwestments", () => {
   const originalData = ref<IInvestmens[]>([]),
      active = ref<IInvestmens>(),
      filters = ref({
         city: "",
         type: "",
         year: ""
      }),
      years = ref<string[]>([]),
      dates = ref<string[]>([]),
      villages = ref<IVilages[]>([]),
      types = ref<Itypes[]>([]),
      totalSum = ref<number>(0)

   function getSumForVillage(village: string, type: number): number {
      let sum = 0
      for (let i = 0; i < originalData.value.length; i++) {
         const data = originalData.value[i]

         if (data.place.name === village && data.type.id === type) {
            sum += data.cost
         }
      }
      return sum
   }

   async function loadData() {
      const respone1 = await client.request(
         readItems("investments" as unknown as string, {
            fields: ["*.*.*"]
         })
      )
      const response2 = await client.request(
         readItems("places" as unknown as string, {
            fields: ["id", "name"]
         })
      )
      const response3 = await client.request(
         readItems("investment_types" as unknown as string, {
            fields: ["id", "name"]
         })
      )
      originalData.value = respone1 as unknown as IInvestmens[]
      villages.value = response2 as unknown as IVilages[]
      types.value = response3 as unknown as IVilages[]

      dates.value = originalData.value.map((item) => item.year)
      dates.value.forEach((date) => {
         if (!years.value.includes(date.substring(0, 4))) {
            years.value.push(date.substring(0, 4))
         }
      })

      originalData.value.forEach((Element) => {
         totalSum.value += Element.cost
         console.log(totalSum.value)
      })
   }

   const data = computed(() => {
      let result = originalData.value
      if (filters.value.city) {
         result = result.filter((item) => item.place.id === parseInt(filters.value.city, 10))
      }
      if (filters.value.type) {
         result = result.filter((item) => item.type.id === parseInt(filters.value.type, 10))
      }
      if (filters.value.year != "") {
         result = result.filter((item) => item.year.substring(0, 4) === filters.value.year)
      }
      return result
   })

   const sum = computed(() => {
      let totalCost = 0
      data.value.forEach((element) => {
         totalCost += element.cost
      })
      return totalCost
   })

   return { data, loadData, active, originalData, filters, sum, years, villages, types, getSumForVillage, totalSum }
})
