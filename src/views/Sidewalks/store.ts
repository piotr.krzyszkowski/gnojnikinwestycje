import { defineStore } from "pinia"
import { readItems } from "@directus/sdk"
import { client } from "@/utils/directusClient"
import { ref } from "vue"
import { Feature } from "geojson"

type LineString = {
   type: "LineString"
   coordinates: number[][]
}

type Geometries = LineString[]

export interface ISidewalks {
   condition: {
      id: number
      name: string
   }
   date_created: string
   date_updated: string
   files: unknown
   id: number
   map:
      | LineString
      | {
           geometries: Geometries
           type: "MultiLineString"
        }
   name: string
   place: {
      id: number
      name: string
   }
   status: {
      id: number
      name: string
   }
}

export const useSidewalks = defineStore("sidewalks", () => {
   const sidewalks = ref<ISidewalks[]>([])
   const lineStringList = ref<Feature[]>([])
   async function fetch() {
      const response = await client.request(
         readItems("sidewalks" as unknown as string, {
            fields: ["*.*.*"],
            filter2: {
               name: {
                  _contains: "Park"
               }
            }
         })
      )

      sidewalks.value = response as unknown as ISidewalks[]
      console.log(sidewalks)
      sidewalks.value.forEach((sidewalk) => {
         if (sidewalk.map.type === "LineString") {
            lineStringList.value.push({
               type: "Feature",
               geometry: {
                  type: "LineString",
                  coordinates: sidewalk.map.coordinates
               },
               properties: {
                  name: sidewalk.name
               }
            })
         } else {
            sidewalk.map.geometries.forEach((geometry) => {
               lineStringList.value.push({
                  type: "Feature",
                  geometry: geometry,
                  properties: {
                     name: sidewalk.name
                  }
               })
            })
         }
      })
      console.log(lineStringList.value)
   }
   return { fetch, sidewalks, lineStringList }
})
