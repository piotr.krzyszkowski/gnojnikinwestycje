import { ref } from "vue"
import { defineStore } from "pinia"
import { readItems } from "@directus/sdk"
import { client } from "@/utils/directusClient"
import { computed } from "vue"
export interface IReservation {
   id?: number
   date: string
   hour_start: string
   hour_end: string
   name: string
   object_id?: {
      id: number
      place: string
      name: string
      color: string
   }
   group_id?: {
      club: string
      color: string
      id: number
      name: string
   }
}

export interface ISportObject {
   id: number
   name: string
   place: string
   color: string
}
export interface IGroup {
   id: number
   club: string
   color: string
   name: string
}

export const useReservation = defineStore("reservations", () => {
   const reservationData = ref<IReservation[]>([]),
      sportObject = ref<ISportObject[]>([]),
      groups = ref<IGroup[]>([]),
      filter = ref<{
         sport_object: string
         group: string
      }>({
         sport_object: "",
         group: ""
      })

   async function loadData() {
      const response = await client.request(
         readItems("sport_reservation", {
            fields: ["*.*"]
         })
      )
      const response2 = await client.request(
         readItems("sport_reservation_objects", {
            fields: ["*"]
         })
      )
      const response3 = await client.request(
         readItems("sport_reservation_groups", {
            fields: ["*.*"]
         })
      )

      reservationData.value = response as unknown as IReservation[]
      sportObject.value = response2 as unknown as ISportObject[]
      groups.value = response3 as unknown as IGroup[]

      sportObject.value.forEach((item) => {
         item.color = item.color.replace("#", "")
      })
   }
   const data = computed(() => {
      let result = reservationData.value

      if (filter.value.sport_object) {
         result = result.filter((item) => item.object_id?.name === filter.value?.sport_object)
      }

      if (filter.value.group) {
         result = result.filter((item) => item.group_id?.name === filter.value?.group)
      }

      return result
   })
   return { loadData, reservationData, sportObject, data, filter, groups }
})
