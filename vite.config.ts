import path from "node:path"

import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import tailwind from "tailwindcss"
import autoprefixer from "autoprefixer"

export default defineConfig({
   css: {
      postcss: {
         plugins: [tailwind(), autoprefixer()]
      }
   },
   plugins: [vue()],
   server: {
      proxy: {
         "/directus": {
            target: "https://orca-app-8y4zd.ondigitalocean.app/",
            changeOrigin: true,
            secure: false,
            rewrite: (path) => path.replace(/^\/directus/, "")
         }
      }
   },
   resolve: {
      alias: {
         "@": path.resolve(__dirname, "./src")
      }
   }
})
